<?php
/*
*   Template Name: Projetos - Galeria 3 colunas
*/

get_header();
?>

<!-- Page Head -->
<?php
get_template_part("banners/default_page_banner");

query_posts('post_format=post-format-gallery');
?>

<!-- Content -->
<div class="container contents blog-page grid-3">
    <div class="row">
        <div class="span12 main-wrap">
            <!-- Main Content -->
            <div class="main">
                <div class="inner-wrapper clearfix">
                    <?php get_template_part("loop");  ?>
                </div>
            </div><!-- End Main Content -->
        </div> <!-- End span9 -->
    </div><!-- End contents row -->
</div><!-- End Content -->

<?php get_footer(); ?>