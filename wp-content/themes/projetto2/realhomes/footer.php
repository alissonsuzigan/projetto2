<?php get_template_part("template-parts/carousel_partners"); ?>

<!-- Start Footer -->
<footer id="footer-wrapper">
    <div id="footer" class="container">
        <div class="row">

            <div class="span4 footer-logo">
             <img src="<?php bloginfo( 'template_url' ); ?>/images/logo-footer.png">
            </div>

            <div class="span5 footer-info">
                <span><i class="icon-pin"></i>Rua Quintino Bocaiuva, 137 - Descanso / Mococa - SP</span><br>
                <span><i class="icon-tel"></i>(19) 3665.8488 / 3656.7170</span><br>
                <span><i class="icon-arroba"></i>atendimento@projetto2.com.br</span><br>
            </div>

            <div class="span3 footer-rede">
                <div>Curta a nossa Fanpage<a href="https://www.facebook.com/projetto2" target="_blank"><i class="icon-face"></i></a></div>
            </div>

        </div>
    </div>
</footer><!-- End Footer -->

<?php
if( !is_user_logged_in() ){
    get_template_part('template-parts/modal-login');
}
?>

<a href="#top" id="scroll-top"><i class="fa fa-chevron-up"></i></a>

<?php wp_footer(); ?>

</body>
</html>