<?php
/*
*   Template Name: Lançamentos - Galeria 2 colunas
*/

get_header();

get_template_part("banners/default_page_banner");

$args = array(
  'post_type' => 'post',
  'tax_query' => array(
    array(
      'taxonomy' => 'post_format',
      'field'    => 'slug',
      'terms'    => array( 'post-format-gallery' ),
      'operator' => 'NOT IN'
    ),
  ),
);
query_posts($args);
?>

<!-- Content -->
<div class="container contents blog-page grid-2">
    <div class="row">
        <div class="span12 main-wrap">
            <!-- Main Content -->
            <div class="main">
                <div class="inner-wrapper clearfix">
                    <?php get_template_part("loop");  ?>
                </div>
            </div><!-- End Main Content -->
        </div> <!-- End span9 -->
    </div><!-- End contents row -->
</div><!-- End Content -->

<?php get_footer(); ?>