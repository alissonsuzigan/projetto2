<section id="contact-form" class="custom-contact inner-wrapper">

  <h3 class="form-heading">Não encontrou o imóvel que procurava? Fale conosco.</h3>


  <form class="contact-form" method="post" action="<?php echo admin_url('admin-ajax.php'); ?>">

    <div class="row-fluid">

      <div class="span6">
        <p>
            <label for="name"><?php _e('Name', 'framework'); ?></label>
            <input type="text" name="name" id="name" class="required" title="<?php _e( '* Please provide your name', 'framework'); ?>">
        </p>
        <p>
            <label for="email"><?php _e('Email', 'framework'); ?></label>
            <input type="text" name="email" id="email" class="email required" title="<?php _e( '* Please provide a valid email address', 'framework'); ?>">
        </p>
        <p>
            <label for="number"><?php _e('Phone Number', 'framework'); ?></label>
            <input type="text" name="number" id="number">
        </p>
      </div>

      <div class="span6">
        <p>
            <label for="comment"><?php _e('Message', 'framework'); ?></label>
            <textarea  name="message" id="comment" class="required" title="<?php _e( '* Please provide your message', 'framework'); ?>"></textarea>
        </p>
        <p>
            <input type="submit" id="submit-button" value="<?php _e('Send Message', 'framework'); ?>" id="submit" class="real-btn" name="submit">
            <img src="<?php echo get_template_directory_uri(); ?>/images/ajax-loader.gif" id="ajax-loader" alt="Loading...">
            <input type="hidden" name="action" value="send_message" />
            <input type="hidden" name="nonce" value="<?php echo wp_create_nonce('send_message_nonce'); ?>"/>
        </p>

        <div id="error-container"></div>
        <div id="message-container">&nbsp;</div>
      </div>
    </div>
  </form>
</section>
