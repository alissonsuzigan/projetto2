<?php
	global $post;
    $format = get_post_format();
    if( false === $format ) {
        $format = 'standard';
    }

    $idx = $wp_query->current_post;
    if( $format === 'standard' ) {
 ?>

        <article <?php post_class(); ?>>
            <?php get_template_part( "post-formats/$format" ); ?>
            <header>
                <h3 class="post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
            </header>
        </article>


<?php } else if( $format === 'gallery' ) {?>


        <article <?php post_class('grid-3 span4x'); ?>>
            <header>
                <h3 class="post-title"><?php the_title(); ?></h3>
            </header>
            <?php get_template_part( "post-formats/$format" ); ?>
        </article>


<?php } ?>
