<?php
if ( has_post_thumbnail() ){
    ?>
    <figure>
        <?php
        if( is_single() ){
            $image_id = get_post_thumbnail_id();
            $image_url = wp_get_attachment_url($image_id);

            the_post_thumbnail( 'full' );
        }else{
            ?>
            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                <?php
                the_post_thumbnail( 'post-2-columns' );
                ?>
            </a>
        <?php } ?>
    </figure>
    <?php
}
?>